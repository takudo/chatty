var mongoose = require('mongoose');

// Default Schemaを取得
var Schema = mongoose.Schema;

//配列に格納する埋め込みドキュメントのスキーマ定義
var CommentsSchema = new Schema({
  title: String,
  body: String,
  date: Date
});

// Defaultのスキーマから新しいスキーマを定義
var PostSchema = new Schema({
  title: String,
  body: String,
  date: Date,
  comments: [CommentsSchema], //配列もOK。ここにコメントモデルが格納される
  metadata: {
    votes: Number,
    favs: Number
  }
});

// モデル化。model('[登録名]', '定義したスキーマクラス')
mongoose.model('Post', PostSchema);


//mongodb://[hostname]/[dbname]
mongoose.connect('mongodb://localhost/blogdb');

//定義したときの登録名で呼び出し
var Post = mongoose.model('Post');

var post = new Post();

//ドット表記で各Fieldへアクセス
post.title = 'test title';
post.body = 'this is a pen';

//埋め込みドキュメントな配列にはpush()を使う
post.comments.push({title: 'comment title', body: 'comment body'});

//pre-hookを定義しているとsave時に定義した関数が走る
post.save(function(err) {
  if(!err) console.log('saved!')
});