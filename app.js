
/**
 * Module dependencies.
 */

var express = require('express'),
    router = require('./core/router'),
    socketio = require('./core/socket.io'),
    http = require('http'),
    path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 8888);
  app.set('views', __dirname + '/app/views');
  app.set('view engine', 'ejs');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

var server = http.createServer(app)
;
//var io = require('socket.io').listen(server);

app.configure('development', function(){
  app.use(express.errorHandler());
});


router.setup(app);

socketio.setup(server, app);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

