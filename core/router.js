var fs = require('fs');

exports.setup = function(app){

    var appBaseDir = './app',
        ctrlBaseDir = appBaseDir + '/ctrl',
        routes = fs.readdirSync(ctrlBaseDir);

    for(var i = 0; i < routes.length; i ++){

        var routeFile = routes[i],
            ctrlName = routeFile.replace(".js", "");

        var ctrl = require('../app/ctrl/' + ctrlName);

        for(var actName in ctrl){

            app.get('/' + ctrlName + '/' + actName, ctrl[actName]);

            if(actName == 'index'){
                app.get('/' + ctrlName, ctrl[actName]);

                if(ctrlName == 'index'){
                    app.get('/', ctrl[actName]);
                }
            }
        }
    }

};

