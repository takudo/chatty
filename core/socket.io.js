
exports.setup = function(server,app){

    console.log(app);

//    var io = require('socket.io').listen(app.listen(8888));
//    var io = require('socket.io').listen(server);
    var io = require('socket.io').listen(app);

    console.log('listen');

    io.sockets.on('connection', function(socket){

        socket.on('chat', function(msg){

            socket.emit('chat', msg);
            socket.broadcast.emit('chat', msg);

        });

    });

}